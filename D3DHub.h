#pragma once

// Win32API Include headers
#include <Windows.h>
#include <tchar.h>

// DirectX 9 Include headers
#include <d3d9.h>
#include <d3dx9.h>

#define DIRECTINPUT_VERSION 0x0800
#define DIRECTINPUT_KEYSTATE_BUFFER 256;
#include <dinput.h>


class D3DHub {

public:
  D3DHub(HINSTANCE hinstance);
  virtual ~D3DHub();

  int Run();

  virtual bool Init();
  virtual void Update(float dt) = 0;
  virtual void Render() = 0;
  virtual bool ReadDevice();

  LRESULT MsgProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);

protected:
  // Win32API attributes
  HWND m_hAppWindow;
  HINSTANCE m_hAppInstance;
  unsigned int m_uiAppWidth;
  unsigned int m_uiAppHeight;
  LPCWSTR m_wcWindowClassName;
  LPCWSTR m_wcWindowTitle;
  DWORD m_dwWindowStyle;
  DWORD m_dwErrorMbStyle;

  // Direct3D attributes
  IDirect3D9* m_pD3D;
  IDirect3DDevice9* m_pD3Ddevice;
  D3DCAPS9 m_D3DdevCaps;
  D3DDISPLAYMODE m_D3Ddm;
  D3DPRESENT_PARAMETERS m_D3Dpp;

  // DirectInput attributes
  IDirectInput8* m_pDI;
  IDirectInputDevice8* m_pDIdevice;
  char m_caKeyStateBuffer[256];

  bool InitWindow();
  bool InitDirect3D();
  bool InitDirectInput();

};