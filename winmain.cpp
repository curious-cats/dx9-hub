#include <Windows.h>

#include "D3DHub.h"
#include "TestApp.h"

int WINAPI WinMain(HINSTANCE hinstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {

  TestApp* pTestApp = new TestApp(hinstance);

  if (!pTestApp->Init())
    return 1;

  return pTestApp->Run();
}
