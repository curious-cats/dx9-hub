#include "D3DHub.h"

namespace { D3DHub* g_pD3DHub; }

LRESULT CALLBACK MainWindowProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam) {
  return g_pD3DHub->MsgProc(hwnd, msg, wParam, lParam);
}

D3DHub::D3DHub(HINSTANCE hInstance) {
  m_hAppInstance = hInstance;
  m_hAppWindow = NULL;
  m_uiAppWidth = 800;
  m_uiAppHeight = 600;
  m_wcWindowClassName = _T("D3DHubWindow");
  m_wcWindowTitle = _T("DirectX 9 Hub");
  m_dwWindowStyle = WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX;
  m_dwErrorMbStyle = MB_ICONEXCLAMATION | MB_OK;

  ZeroMemory(m_caKeyStateBuffer, sizeof(m_caKeyStateBuffer));
}

D3DHub::~D3DHub() {}

int D3DHub::Run() {
  MSG msg = {0};

  while (msg.message != WM_QUIT) {
    if (!PeekMessage(&msg, NULL, NULL, NULL, PM_REMOVE)) {
      Update(0.0f);
      Render();
      ReadDevice();

      if (m_caKeyStateBuffer[DIK_ESCAPE] & 0x80)
        break;
    }

    TranslateMessage(&msg);
    DispatchMessage(&msg);
  }

  return static_cast<int>(msg.wParam);
}

bool D3DHub::Init() {
  if (!InitWindow()) 
    return false;
  
  if (!InitDirect3D()) 
    return false;

  if (!InitDirectInput())
    return false;

  return true;
}

bool D3DHub::InitWindow() {
  WNDCLASSEX wcex;
  int result;

  ZeroMemory(&wcex, sizeof(WNDCLASSEX));

  // Setup window
  wcex.cbClsExtra = 0;
  wcex.cbWndExtra = 0;
  wcex.style = CS_HREDRAW | CS_VREDRAW;
  wcex.hInstance = m_hAppInstance;
  wcex.lpfnWndProc = MainWindowProc;
  wcex.cbSize = sizeof(WNDCLASSEX);
  wcex.hIcon = LoadIcon(NULL, IDI_APPLICATION);
  wcex.hCursor = LoadCursor(NULL, IDC_CROSS);
  wcex.hbrBackground = (HBRUSH) GetStockObject(NULL_BRUSH);
  wcex.lpszMenuName = NULL;
  wcex.lpszClassName = m_wcWindowClassName;
  wcex.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

  // Register window class
  result = RegisterClassEx(&wcex);

  if (!result) {
    MessageBox(NULL, _T("Failed to register window class with RegisterClassEx()"), 
      _T("WIN32API: Error"), m_dwErrorMbStyle);

    return false;
  }

  // Cache requested width and height
  RECT r = { 0, 0, m_uiAppWidth, m_uiAppHeight };
  AdjustWindowRect(&r, m_dwWindowStyle, false);
  int width = r.right - r.left;
  int height = r.bottom - r.top;

  // Create window
  m_hAppWindow = CreateWindowEx(0, m_wcWindowClassName, m_wcWindowTitle, m_dwWindowStyle,
    GetSystemMetrics(SM_CXSCREEN)/2 - width/2, GetSystemMetrics(SM_CYSCREEN)/2 - width/2,
    width, height, NULL, NULL, m_hAppInstance, NULL);

  if (!m_hAppWindow) {
    MessageBox(NULL, _T("Failed to create window with CreateWindow()"),
      _T("WIN32API: Error"), m_dwErrorMbStyle);

    return false;
  }

  ShowWindow(m_hAppWindow, SW_SHOW);
  UpdateWindow(m_hAppWindow);

  return true;
}

bool D3DHub::InitDirect3D() {
  int iVertProc = NULL;
  HRESULT hResult = NULL;

  // Getting COM to Direct3D
  m_pD3D = Direct3DCreate9(D3D_SDK_VERSION);

  if (!m_pD3D) {
    MessageBox(NULL, _T("Failed to create Direct3D COM Object reference"), 
      _T("D3D: Error"), m_dwErrorMbStyle);

    return false;
  }

  // Check device capabilities
  hResult = m_pD3D->GetDeviceCaps(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, &m_D3DdevCaps);
  if (FAILED(hResult)) {
    MessageBox(NULL, _T("Failed to get device capabilities"),
      _T("D3D: Error"), m_dwErrorMbStyle);

    return false;
  }

  // Set hard- or software vertex processing based upon device capabilities
  iVertProc = (m_D3DdevCaps.DevCaps & D3DDEVCAPS_HWTRANSFORMANDLIGHT) ?
    D3DCREATE_HARDWARE_VERTEXPROCESSING : D3DCREATE_SOFTWARE_VERTEXPROCESSING;

  // Getting available display modes for backbuffer
  ZeroMemory(&m_D3Ddm, sizeof(D3DDISPLAYMODE));
  hResult = m_pD3D->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &m_D3Ddm);

  if (FAILED(hResult)) {
    MessageBox(NULL, _T("Failed to get available display modes for backbuffer"),
      _T("D3D: Error"), m_dwErrorMbStyle);

    return false;
  }

  // todo: Matching depth-stencil format with render-target format

  // Setup present parameters
  ZeroMemory(&m_D3Dpp, sizeof(D3DPRESENT_PARAMETERS));
  m_D3Dpp.BackBufferWidth = m_uiAppWidth;
  m_D3Dpp.BackBufferHeight = m_uiAppHeight;
  m_D3Dpp.Windowed = true;
  m_D3Dpp.BackBufferCount = 1;
  m_D3Dpp.BackBufferFormat = m_D3Ddm.Format;
  m_D3Dpp.MultiSampleType = D3DMULTISAMPLE_NONE; // No AA
  m_D3Dpp.MultiSampleQuality = 0;
  m_D3Dpp.hDeviceWindow = m_hAppWindow;
  m_D3Dpp.Flags = 0;
  m_D3Dpp.EnableAutoDepthStencil = true;
  // The stencil format D3DFMT_D16 will be used, because D3DFMT_D24S8 
  // seems to be not compatible with the current render target.
  m_D3Dpp.AutoDepthStencilFormat = D3DFMT_D16; 
  m_D3Dpp.FullScreen_RefreshRateInHz = D3DPRESENT_RATE_DEFAULT;
  m_D3Dpp.PresentationInterval = D3DPRESENT_INTERVAL_IMMEDIATE;
  m_D3Dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;

  // Create Direct3D Device
  hResult = m_pD3D->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, m_hAppWindow, 
    iVertProc, &m_D3Dpp, &m_pD3Ddevice);

  if (FAILED(hResult)) {
    MessageBox(NULL, _T("Failed to create Direct3D device"), 
      _T("D3D: Error"), m_dwErrorMbStyle);

    return false;
  }

  return true;
}

bool D3DHub::InitDirectInput() {
  HRESULT hResult = NULL;
  int cooperation = DISCL_FOREGROUND | DISCL_NONEXCLUSIVE;

  // Creating DirectInput COM reference
  hResult = DirectInput8Create(m_hAppInstance, DIRECTINPUT_VERSION, 
    IID_IDirectInput8, (void**) &m_pDI, NULL);

  if (FAILED(hResult)) {
    MessageBox(NULL, _T("Failed to create DirectInput COM object reference"),
      _T("DI: Error"), m_dwErrorMbStyle);
  }

  // Create DirectInput device with default GUID of systems keyboard
  hResult = m_pDI->CreateDevice(GUID_SysKeyboard, &m_pDIdevice, NULL);

  if (FAILED(hResult)) {
    MessageBox(NULL, _T("Failed to create DirectInput device"),
      _T("DI: Error"), m_dwErrorMbStyle);

    return false;
  }

  // Setup data format to predefined structure for keyboard input
  hResult = m_pDIdevice->SetDataFormat(&c_dfDIKeyboard);

  if (FAILED(hResult)) {
    MessageBox(NULL, _T("Failed to set data format for device"),
      _T("DI: Error"), m_dwErrorMbStyle);
    m_pDIdevice->Release();

    return false;
  }

  // Setup cooperation level
  hResult = m_pDIdevice->SetCooperativeLevel(m_hAppWindow, cooperation);

  if (FAILED(hResult)) {
    MessageBox(NULL, _T("Failed to set cooperative level for device"),
      _T("DI: Error"), m_dwErrorMbStyle);
    m_pDIdevice->Release();

    return false;
  }

  // Aquire the device
  hResult = m_pDIdevice->Acquire();

  if (FAILED(hResult)) {
    MessageBox(NULL, _T("Failed to aquire the device"),
      _T("DI: Error"), m_dwErrorMbStyle);
    m_pDIdevice->Release();

    return false;
  }
  
  return true;
}

bool D3DHub::ReadDevice() {
  HRESULT hResult;

  while (true) {
    // Poll DirectInput device
    m_pDIdevice->Poll();

    // Read device state
    hResult = m_pDIdevice->GetDeviceState(sizeof(m_caKeyStateBuffer), (LPVOID) m_caKeyStateBuffer);

    if (SUCCEEDED(hResult)) 
      break;
    
    if (hResult != DIERR_INPUTLOST && hResult != DIERR_NOTACQUIRED)
      return false;

    hResult = m_pDIdevice->Acquire();

    if (FAILED(hResult)) 
      return false;

  }

  return true;
}

LRESULT D3DHub::MsgProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam) {
  switch (msg) {
    case WM_DESTROY:
      DestroyWindow(hwnd);
      break;
    
    case WM_CLOSE:
      PostQuitMessage(0);
      break;
    
    default:
      return DefWindowProc(hwnd, msg, wParam, lParam);
  }

  return 0;
}