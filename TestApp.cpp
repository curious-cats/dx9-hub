#include "TestApp.h"

TestApp::TestApp(HINSTANCE hInstance):D3DHub(hInstance) {  }
TestApp::~TestApp() { }

bool TestApp::Init() { 
  if (!D3DHub::Init())
    return false;

  return true;
}

void TestApp::Update(float dt) { }
void TestApp::Render() { 
  m_pD3Ddevice->Clear(0, NULL, D3DCLEAR_TARGET, D3DCOLOR_XRGB(255, 0, 0), 1.0f, 0);
  m_pD3Ddevice->Present(NULL, NULL, NULL, NULL);
}