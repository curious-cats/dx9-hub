#pragma once

#include "D3DHub.h"

class TestApp : public D3DHub {

  public:
    TestApp(HINSTANCE hInstance);
    ~TestApp();

    bool Init() override;
    void Update(float dt) override;
    void Render() override;

};

